<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        App\User::create(
            [
                'name' => 'Lino',
                'firstName' => 'Alves',
                'phone' => '+22579850072',
                'email' => 'dupont@la.fr',
                'password' => bcrypt('pass'),
            ]
        );
    }
}
